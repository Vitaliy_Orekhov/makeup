// Custom scripts
$(document).ready(function () {

    // MetsiMenu
    $('#side-menu').metisMenu();
 
    // Collapse ibox function
    $('.collapse-link:not(.binded)').addClass("binded").click( function() {
        var ibox = $(this).closest('div.ibox');
        var button = $(this).find('i');
        var content = ibox.find('div.ibox-content');
        content.slideToggle(200);
        button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
        ibox.toggleClass('').toggleClass('border-bottom');
        setTimeout(function () {
            ibox.resize();
            ibox.find('[id^=map-]').resize();
        }, 50);
    });
 
    // Close ibox function
    $('.close-link:not(.binded)').addClass("binded").click( function() {
        var content = $(this).closest('div.ibox');
        content.remove();
    });
 
    // Small todo handler
    $('.check-link:not(.binded)').addClass("binded").click( function(){
        var button = $(this).find('i');
        var label = $(this).next('span');
        button.toggleClass('fa-check-square').toggleClass('fa-square-o');
        label.toggleClass('todo-completed');
        return false;
    });
 
    // Append config box / Only for demo purpose
    /*
    $.get("skin-config.html", function (data) {
        $('body').append(data);
    });
    */
 
    // minimalize menu
    $('.navbar-minimalize:not(.binded)').addClass("binded").click(function () {
        $("body").toggleClass("mini-navbar");
        SmoothlyMenu();
    })
 
    // tooltips
    $('.tooltip-demo').tooltip({
        selector: "[data-toggle=tooltip]",
        container: "body"
    })
 
    // Move modal to body
    // Fix Bootstrap backdrop issu with animation.css
    $('.modal').appendTo("body")
 
    // Full height of sidebar
    function fix_height() {
        var heightWithoutNavbar = $("body > #wrapper").height() - 61;
        $(".sidebard-panel").css("min-height", heightWithoutNavbar + "px");
    }
    fix_height();
 
    // Fixed Sidebar
    // unComment this only whe you have a fixed-sidebar
            //    $(window).bind("load", function() {
            //        if($("body").hasClass('fixed-sidebar')) {
            //            $('.sidebar-collapse').slimScroll({
            //                height: '100%',
            //                railOpacity: 0.9,
            //            });
            //        }
            //    })
 
    $(window).bind("load resize click scroll", function() {
        if(!$("body").hasClass('body-small')) {
            fix_height();
        }
    })
 
    $("[data-toggle=popover]")
        .popover();

    /*---Tooltips---*/
    $(".hover_tooltip").mousemove(function(e){
        var p;
        var elem;
        var text = $(this).data("tooltip");
        if($(this).data("x") === 'left'){
            p = 'left';
        }else{
            p = 'right';
        }
        if($(this).data("y") === 'bottom'){
            p += 'bottom';
        }else{
            p += 'top';
        }
        switch(p){
            case 'leftbottom':
                elem = $("#bottom_left_hover_tooltip");
                var w = elem.width();
                var h = elem.height();
                elem.css('top', e.clientY+h+14).css('left', e.clientX-w-20);
                break;
            case 'lefttop':
                elem = $("#top_left_hover_tooltip");
                var w = elem.width();
                var h = elem.height();
                elem.css('top', e.clientY-h-34).css('left', e.clientX-w-20);
                break;
            case 'rightbottom':
                elem = $("#bottom_right_hover_tooltip");
                var h = elem.height();
                elem.css('top', e.clientY+h+14).css('left', e.clientX-2);
                break;
            default:
                elem = $("#top_right_hover_tooltip");
                var h = elem.height();
                elem.css('top', e.clientY-h-34).css('left', e.clientX-12);
        }
        elem.text(text)
        elem.show();
        if($(this).data("color") === "blue"){
            elem.addClass("tooltip-blue");
        }
    }).mouseout(function(){
        var p;
        var elem;
        if($(this).data("x") === 'left'){
            p = 'left';
        }else{
            p = 'right';
        }
        if($(this).data("y") === 'bottom'){
            p += 'bottom';
        }else{
            p += 'top';
        }
        switch(p){
            case 'leftbottom':
                elem = $("#bottom_left_hover_tooltip");
                break;
            case 'lefttop':
                elem = $("#top_left_hover_tooltip");
                break;
            case 'rightbottom':
                elem = $("#bottom_right_hover_tooltip");
                break;
            default:
                elem = $("#top_right_hover_tooltip");
        }
        elem.hide();
        elem.removeClass("tooltip-blue");
    });
});


// For demo purpose - animation css script
window.animationHover = function(element, animation){
    element = $(element);
    element.hover(
        function() {
            element.addClass('animated ' + animation);
        },
        function(){
            //wait for animation to finish before removing classes
            window.setTimeout( function(){
                element.removeClass('animated ' + animation);
            }, 2000);
        });
};

// Minimalize menu when screen is less than 768px
$(function() {
    $(window).bind("load resize", function() {
        if ($(this).width() < 769) {
            $('body').addClass('body-small')
        } else {
            $('body').removeClass('body-small')
        }
    })
})

window.SmoothlyMenu = function() {
    if (!$('body').hasClass('mini-navbar') || $('body').hasClass('body-small')) {
        // Hide menu in order to smoothly turn on when maximize menu
        $('#side-menu').hide();
        // For smoothly turn on menu
        setTimeout(
            function () {
                $('#side-menu').fadeIn(500);
            }, 100);
    } else if ($('body').hasClass('fixed-sidebar')){
        $('#side-menu').hide();
        setTimeout(
            function () {
                $('#side-menu').fadeIn(500);
            }, 300);
    } else {
        // Remove all inline style from jquery fadeIn function to reset menu state
        $('#side-menu').removeAttr('style');
    }
};

// Dragable panels
window.WinMove = function() {
    var element = "[class*=col]";
    var handle = ".ibox-title";
    var connect = "[class*=col]";
    $(element).sortable(
        {
            handle: handle,
            connectWith: connect,
            tolerance: 'pointer',
            forcePlaceholderSize: true,
            opacity: 0.8,
        })
        .disableSelection();
};

function aboutBlockToogle(){
    var about = $('#about_block');
    if(about.css('display') == 'none'){
        about.slideDown(500);
    }else{
        about.slideUp(500);
    }
};

function registerFormToogle(){
    var reg = $('#register_form');
    if(reg.css('display') == 'none'){
        reg.slideDown(500);
    }else{
        reg.slideUp(500);
    }
};

function authFormToogle(){
    var auth = $('#auth_form');
    if(auth.css('display') == 'none'){
        auth.slideDown(500);
    }else{
        auth.slideUp(500);
    }
};

function recovFormToogle(){
    var auth = $('#recov_form');
    if(auth.css('display') == 'none'){
        auth.slideDown(500);
    }else{
        auth.slideUp(500);
    }
};

function notifFormToogle(){
    var auth = $('#notif_form');
    if(auth.css('display') == 'none'){
        auth.slideDown(500);
    }else{
        auth.slideUp(500);
    }
};

function footerResize(){
    var f = $('.footer');
    var heightFooter = parseInt(f.height());
    f.css('marginTop', '-' + heightFooter + 'px');
    $('#wrap').css('paddingBottom', heightFooter + 'px');
}