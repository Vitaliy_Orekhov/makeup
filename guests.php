<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Мои гости</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Toastr style -->
    <link href="css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <!-- Gritter -->
    <link href="js/plugins/gritter/jquery.gritter.css" rel="stylesheet">

    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

</head>

<body>
    <div id="wrapper">
        <div class="container-fluid">
            <nav class="navbar navbar-static-top row border-bottom-blue" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">
                        <img src="img/logo.png" alt="Logo">
                    </a>
                    <button type="button" class="navbar-toggle btn btn-default btn-circle btn-lg btn-social pull-right" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"><i class="fa fa-share-alt" aria-hidden="true"></i></button>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-top-links navbar-right mr0">
                        <li class="contact">
                            <a href="tel:+380956475410"><span>[phone]</span>+380956475410</a>
                        </li>
                        <li class="contact">
                            <a href="mailto:andriiusikov@gmail.com"><span>[email]</span>andriiusikov@gmail.com</a>
                        </li>
                        <li class="social">
                            <a href="" class="btn btn-default btn-circle btn-lg btn-social hover_tooltip" data-x="left" data-y="bottom" data-tooltip="Поделиться через ВКонтакте"><i class="fa fa-vk" aria-hidden="true"></i></a><span class="visible-xs">Поделиться ВКонтакте</span>
                        </li><!--
                        --><li class="social">
                            <a href="" class="btn btn-default btn-circle btn-lg btn-social hover_tooltip" data-x="left" data-y="bottom" data-tooltip="Поделиться через Facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a><span class="visible-xs">Поделиться на Facebook</span>
                        </li><!--
                        --><li class="social">
                            <a href="" class="btn btn-default btn-circle btn-lg btn-social hover_tooltip" data-x="left" data-y="bottom" data-tooltip="Поделиться через Instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a><span class="visible-xs">Поделиться в Instagram</span>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>

        <div id="wrap">
            <nav class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="nav-header">
                            <div class="dropdown profile-element">
                                <img alt="avatar" class="img-circle" src="img/a3.jpg" />
                                <div>
                                    <span class="user-name text-blue">Екатерина</span>
                                    <span>Днепропетровск</span>
                                    <span>29 лет</span>
                                </div>
                                <input type="button" name="" class="btn btn-info btn-rounded btn-xl btn-block mt20" value="Забронировать билет">
                            </div>
                        </li>
                        <li>
                            <a href="#">Мои поездки</a>
                        </li>
                        <li>
                            <a href="#">Поиск попутчиков</a>
                        </li>
                        <li>
                            <a href="#">Мои попутчики</a>
                        </li>
                        <li>
                            <a href="#">Избранные пользователи</a>
                        </li>
                        <li>
                            <a href="#">Вас пригласили</a>
                        </li>
                        <li class="check active">
                            <a href="#">Мои гости</a>
                        </li>
                        <li class="check">
                            <a href="#">Сообщения</a>
                        </li>
                        <li>
                            <a href="#">Отзывы</a>
                        </li>
                        <li>
                            <a href="#">О нашем сервисе</a>
                        </li>
                        <li>
                            <a href="#">Настройки профиля</a>
                        </li>
                        <li>
                            <a href="#">Выход</a>
                        </li>
                    </ul>

                </div>
            </nav>

            <div id="page-wrapper" class="gray-bg dashbard">
                <div class="flex-item-1">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="wrapper wrapper-content">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h2 class="page-title mb20">О нашем сервисе</h2>
                                        <p class="text-helv-roman mb20">В этом разделе находятся пользователи, которые просматривали Ваш профиль</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <div class="ibox">
                                            <div class="ibox-content">
                                                <div class="avatar" style="background-image: url(./img/ment.jpg);">
                                                    <div>
                                                        <p class="user-info">
                                                            <span class="user-name">Екатерина</span>
                                                            <span>Днепропетровск, 21 лет</span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <p class="rating mt5">Рейтинг попутчика:
                                                    <span class="block pull-right">
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                    </span>
                                                </p>
                                                <a href="#" class="btn btn-default btn-circle btn-lg btn-icon pull-right"><i class="fa fa-heart" aria-hidden="true"></i></a>
                                                <div class="mr60">
                                                    <a href="#" class="btn btn-info btn-rounded btn-lg btn-block mr60">Пригласить</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <div class="ibox">
                                            <div class="ibox-content">
                                                <div class="avatar" style="background-image: url(./img/2-9.jpg);">
                                                    <div>
                                                        <p class="user-info">
                                                            <span class="user-name">Антонио</span>
                                                            <span>Челябинск, 33 лет</span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <p class="rating mt5">Рейтинг попутчика:
                                                    <span class="block pull-right">
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                    </span>
                                                </p>
                                                <a href="#" class="btn btn-default btn-circle btn-lg btn-icon pull-right"><i class="fa fa-heart" aria-hidden="true"></i></a>
                                                <div class="mr60">
                                                    <a href="#" class="btn btn-info btn-rounded btn-lg btn-block mr60">Пригласить</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <div class="ibox">
                                            <div class="ibox-content">
                                                <div class="avatar" style="background-image: url(./img/123.jpg);">
                                                    <div>
                                                        <p class="user-info">
                                                            <span class="user-name">Джонни</span>
                                                            <span>Питтсбург, 38 лет</span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <p class="rating mt5">Рейтинг попутчика:
                                                    <span class="block pull-right">
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                    </span>
                                                </p>
                                                <a href="#" class="btn btn-default btn-circle btn-lg btn-icon pull-right"><i class="fa fa-heart" aria-hidden="true"></i></a>
                                                <div class="mr60">
                                                    <a href="#" class="btn btn-info btn-rounded btn-lg btn-block mr60">Пригласить</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <div class="ibox">
                                            <div class="ibox-content">
                                                <div class="avatar" style="background-image: url(./img/ment.jpg);">
                                                    <div>
                                                        <p class="user-info">
                                                            <span class="user-name">Екатерина</span>
                                                            <span>Днепропетровск, 21 лет</span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <p class="rating mt5">Рейтинг попутчика:
                                                    <span class="block pull-right">
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                    </span>
                                                </p>
                                                <a href="#" class="btn btn-default btn-circle btn-lg btn-icon pull-right"><i class="fa fa-heart" aria-hidden="true"></i></a>
                                                <div class="mr60">
                                                    <a href="#" class="btn btn-info btn-rounded btn-lg btn-block mr60">Пригласить</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <div class="ibox">
                                            <div class="ibox-content">
                                                <div class="avatar" style="background-image: url(./img/2-9.jpg);">
                                                    <div>
                                                        <p class="user-info">
                                                            <span class="user-name">Антонио</span>
                                                            <span>Челябинск, 33 лет</span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <p class="rating mt5">Рейтинг попутчика:
                                                    <span class="block pull-right">
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                    </span>
                                                </p>
                                                <a href="#" class="btn btn-default btn-circle btn-lg btn-icon pull-right"><i class="fa fa-heart" aria-hidden="true"></i></a>
                                                <div class="mr60">
                                                    <a href="#" class="btn btn-info btn-rounded btn-lg btn-block mr60">Пригласить</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <div class="ibox">
                                            <div class="ibox-content">
                                                <div class="avatar" style="background-image: url(./img/123.jpg);">
                                                    <div>
                                                        <p class="user-info">
                                                            <span class="user-name">Джонни</span>
                                                            <span>Питтсбург, 38 лет</span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <p class="rating mt5">Рейтинг попутчика:
                                                    <span class="block pull-right">
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                    </span>
                                                </p>
                                                <a href="#" class="btn btn-default btn-circle btn-lg btn-icon pull-right"><i class="fa fa-heart" aria-hidden="true"></i></a>
                                                <div class="mr60">
                                                    <a href="#" class="btn btn-info btn-rounded btn-lg btn-block mr60">Пригласить</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <div class="ibox">
                                            <div class="ibox-content">
                                                <div class="avatar" style="background-image: url(./img/ment.jpg);">
                                                    <div>
                                                        <p class="user-info">
                                                            <span class="user-name">Екатерина</span>
                                                            <span>Днепропетровск, 21 лет</span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <p class="rating mt5">Рейтинг попутчика:
                                                    <span class="block pull-right">
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                    </span>
                                                </p>
                                                <a href="#" class="btn btn-default btn-circle btn-lg btn-icon pull-right"><i class="fa fa-heart" aria-hidden="true"></i></a>
                                                <div class="mr60">
                                                    <a href="#" class="btn btn-info btn-rounded btn-lg btn-block mr60">Пригласить</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <div class="ibox">
                                            <div class="ibox-content">
                                                <div class="avatar" style="background-image: url(./img/2-9.jpg);">
                                                    <div>
                                                        <p class="user-info">
                                                            <span class="user-name">Антонио</span>
                                                            <span>Челябинск, 33 лет</span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <p class="rating mt5">Рейтинг попутчика:
                                                    <span class="block pull-right">
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                    </span>
                                                </p>
                                                <a href="#" class="btn btn-default btn-circle btn-lg btn-icon pull-right"><i class="fa fa-heart" aria-hidden="true"></i></a>
                                                <div class="mr60">
                                                    <a href="#" class="btn btn-info btn-rounded btn-lg btn-block mr60">Пригласить</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <div class="ibox">
                                            <div class="ibox-content">
                                                <div class="avatar" style="background-image: url(./img/123.jpg);">
                                                    <div>
                                                        <p class="user-info">
                                                            <span class="user-name">Джонни</span>
                                                            <span>Питтсбург, 38 лет</span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <p class="rating mt5">Рейтинг попутчика:
                                                    <span class="block pull-right">
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                    </span>
                                                </p>
                                                <a href="#" class="btn btn-default btn-circle btn-lg btn-icon pull-right"><i class="fa fa-heart" aria-hidden="true"></i></a>
                                                <div class="mr60">
                                                    <a href="#" class="btn btn-info btn-rounded btn-lg btn-block mr60">Пригласить</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer flex-footer">
        <div>
            &copy; intrain.club - 2017. Все права защищены. Правообладатель: Усиков А.А.
        </div>
        <div>
            Разработка сайта: Творческая студия "<a href="#">YB Desing</a>"
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="js/jquery-2.1.1.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Flot -->
    <script src="js/plugins/flot/jquery.flot.js"></script>
    <script src="js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="js/plugins/flot/jquery.flot.spline.js"></script>
    <script src="js/plugins/flot/jquery.flot.resize.js"></script>
    <script src="js/plugins/flot/jquery.flot.pie.js"></script>

    <!-- Peity -->
    <script src="js/plugins/peity/jquery.peity.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="js/inspinia.js"></script>
    <script src="js/plugins/pace/pace.min.js"></script>

    <!-- jQuery UI -->
    <script src="js/plugins/jquery-ui/jquery-ui.min.js"></script>

    <!-- GITTER -->
    <script src="js/plugins/gritter/jquery.gritter.min.js"></script>

    <!-- Sparkline -->
    <script src="js/plugins/sparkline/jquery.sparkline.min.js"></script>

    <!-- ChartJS-->
    <script src="js/plugins/chartJs/Chart.min.js"></script>

    <!-- Toastr -->
    <script src="js/plugins/toastr/toastr.min.js"></script>


    <script>
        $(document).ready(function() {
            footerResize();
        });

        $(window).resize(function() {
            footerResize();
        });
    </script>
    <!--Tooltip Start-->
    <div id="top_left_hover_tooltip"></div>
    <div id="top_right_hover_tooltip"></div>
    <div id="bottom_left_hover_tooltip"></div>
    <div id="bottom_right_hover_tooltip"></div>
    <!--Tooltip End-->
</body>
</html>
