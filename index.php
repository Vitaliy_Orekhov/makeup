<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>inTrain</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Toastr style -->
    <link href="css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <!-- Gritter -->
    <link href="js/plugins/gritter/jquery.gritter.css" rel="stylesheet">

    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <style type="text/css">
        .footer {
            bottom: 0;
            left: 0;
            position: absolute;
            right: 0;
        }
    </style>

</head>

<body>
    <div id="wrapper">
        <div class="container-fluid">
            <nav class="navbar navbar-static-top row border-bottom-blue" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">
                        <img src="img/logo.png" alt="Logo">
                    </a>
                    <button type="button" class="navbar-toggle btn btn-default btn-circle btn-lg btn-social pull-right" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"><i class="fa fa-share-alt" aria-hidden="true"></i></button>
                    <a class="btn btn-default btn-circle btn-lg btn-about hover_tooltip" href="#" data-x="right" data-y="bottom" data-tooltip="О нас" onclick="aboutBlockToogle()">?</a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	                <ul class="nav navbar-top-links navbar-right mr0">
	                    <li class="social">
	                        <a href="" class="btn btn-default btn-circle btn-lg btn-social hover_tooltip" data-x="left" data-y="bottom" data-tooltip="Поделиться через Viber"><i class="fa fa-whatsapp" aria-hidden="true"></i></a><span class="visible-xs">Поделиться через Viber</span>
	                    </li><!--
	                    --><li class="social">
	                        <a href="" class="btn btn-default btn-circle btn-lg btn-social hover_tooltip" data-x="left" data-y="bottom" data-tooltip="Поделиться через ВКонтакте"><i class="fa fa-vk" aria-hidden="true"></i></a><span class="visible-xs">Поделиться ВКонтакте</span>
	                    </li><!--
	                    --><li class="social">
	                        <a href="" class="btn btn-default btn-circle btn-lg btn-social hover_tooltip" data-x="left" data-y="bottom" data-tooltip="Поделиться через Facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a><span class="visible-xs">Поделиться на Facebook</span>
	                    </li><!--
	                    --><li class="social">
	                        <a href="" class="btn btn-default btn-circle btn-lg btn-social hover_tooltip" data-x="left" data-y="bottom" data-tooltip="Поделиться через Instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a><span class="visible-xs">Поделиться в Instagram</span>
	                    </li>
	                </ul>
	            </div>
            </nav>
        </div>

        <div id="wrap">
            <div id="tp-banner" class="tp-banner-container">
                <div class="tp-banner">
                    <ul>
                        <li data-transition="zoomin" data-slotamount="10" data-masterspeed="1000"  class="slide-1">
                            <img src="img/slide/slide1.jpg"  alt="slidebg1"  data-bgfit="cover" data-bgposition="center top" data-bgrepeat="no-repeat">
                            <div class="tp-caption caption-title uppercase skewfromleft"
                                data-start="500"
                                data-speed="500"
                                data-easing="Linear.easeNone">
                                Ищите попутчиков
                            </div>
                            <div class="tp-caption caption-list caption-list-1 skewfromleft"
                                data-start="800"
                                data-speed="500"
                                data-easing="Linear.easeNone">
                                Хватит играть в лотерею!
                            </div>
                            <div class="tp-caption caption-list caption-list-2 skewfromleft"
                                data-start="1000"
                                data-speed="500"
                                data-easing="Linear.easeNone">
                                Планируйте сами, с кем путешествовать!
                            </div>
                            <button href="" class="btn btn-success btn-rounded btn-xl tp-caption caption-btn caption-btn-1 sfb"
                                data-start="1000"
                                data-speed="500"
                                data-easing="Linear.easeNone"
                                onclick="registerFormToogle();">
                                Присоедититься
                            </button>
                            <button href="" class="btn btn-info btn-rounded btn-xl tp-caption caption-btn caption-btn-2 sfb"
                                data-start="1100"
                                data-speed="500"
                                data-easing="Linear.easeNone"
                                onclick="authFormToogle();">
                                Найти попутчиков
                            </button>
                        </li>
                        <li data-transition="zoomin" data-slotamount="10" data-masterspeed="1000"  class="slide-2">
                            <img src="img/slide/slide2.jpg"  alt="slidebg2"  data-bgfit="cover" data-bgposition="center top" data-bgrepeat="no-repeat">
                            <div class="tp-caption caption-title uppercase skewfromleft"
                                data-start="500"
                                data-speed="500"
                                data-easing="Linear.easeNone">
                                Присоединяйтесь
                            </div>
                            <div class="tp-caption caption-list caption-list-1 skewfromleft"
                                data-start="800"
                                data-speed="200"
                                data-easing="Linear.easeNone">
                                Наш сервис инновационный и уникальный.
                            </div>
                            <div class="tp-caption caption-list caption-list-2 skewfromleft"
                                data-start="900"
                                data-speed="200"
                                data-easing="Linear.easeNone">
                                Мы ломаем стереотипы и создаем новую культуру
                            </div>
                            <div class="tp-caption caption-list caption-list-3 skewfromleft"
                                data-start="1000"
                                data-speed="200"
                                data-easing="Linear.easeNone">
                                планирования поездок и путешествий! 
                            </div>
                            <button href="" class="btn btn-success btn-rounded btn-xl tp-caption caption-btn caption-btn-1 sfb"
                                data-start="1000"
                                data-speed="500"
                                data-easing="Linear.easeNone"
                                onclick="registerFormToogle();">
                                Присоедититься
                            </button>
                            <button href="" class="btn btn-info btn-rounded btn-xl tp-caption caption-btn caption-btn-2 sfb"
                                data-start="1100"
                                data-speed="500"
                                data-easing="Linear.easeNone"
                                onclick="authFormToogle();">
                                Найти попутчиков
                            </button>
                        </li>
                        <li data-transition="zoomin" data-slotamount="10" data-masterspeed="1000" class="slide-3">
                            <img src="img/slide/slide3.jpg"  alt="slidebg1"  data-bgfit="cover" data-bgposition="center top" data-bgrepeat="no-repeat">
                            <div class="tp-caption caption-title uppercase skewfromleft"
                                data-start="500"
                                data-speed="500"
                                data-easing="Linear.easeNone">
                                Путешествуйте с нами
                            </div>
                            <div class="tp-caption caption-list caption-list-1 skewfromleft"
                                data-start="800"
                                data-speed="200"
                                data-easing="Linear.easeNone">
                                inTrain- клуб попутчиков по интересам, созданный для совместных путешествий поездами.
                            </div>
                            <div class="tp-caption caption-list caption-list-2 skewfromleft"
                                data-start="1000"
                                data-speed="200"
                                data-easing="Linear.easeNone">
                                Присоединяйтесь, выбирайте попутчиков и путешествуйте с нами!
                            </div>
                            <button href="" class="btn btn-success btn-rounded btn-xl tp-caption caption-btn caption-btn-1 sfb"
                                data-start="1000"
                                data-speed="500"
                                data-easing="Linear.easeNone"
                                onclick="registerFormToogle();">
                                Присоедититься
                            </button>
                            <button href="" class="btn btn-info btn-rounded btn-xl tp-caption caption-btn caption-btn-2 sfb"
                                data-start="1100"
                                data-speed="500"
                                data-easing="Linear.easeNone"
                                onclick="authFormToogle();">
                                Найти попутчиков
                            </button>
                        </li>
                    </ul>
                </div>
            </div><!--#tp-banner-->
            <div id="about_block" class="about-block" style="display: none;">
                <button class="close_btn" onclick="aboutBlockToogle()"></button>
                <div class="about-block-body">
                    <h1 class="uppercase title-blue mt0 mb40">О нашем сервисе</h1>
                    <p>
                        Люблю путешествовать комфортно и безопасно. Поэтому, планируя поездку, всегда выбираю поезд.<br>
                        Определяюсь с датой, покупаю билет, собираю чемодан и отправляюсь на вокзал.<br>
                        И когда я уверен, что все предусмотрел, вдруг понимаю, что ни чего не знаю о своих попутчиках. И меня могут ждать не совсем приятные сюрпризы…
                    </p>
                    <p>
                        Знакомая ситуация?<br>
                        Хватит играть в лотерею! Пора самому планировать с кем путешествовать! 
                    </p>
                    <p>
                        Мы ломаем стереотипы и создаем новую культуру планирования поездок и путешествий!<br>In Train- это не просто уникальный интернет-сервис поиска попутчиков.<br>
                        In Train- это клуб попутчиков, созданный, что бы объединить людей, путешествующих поездами.<br>
                        Клуб, в котором Вы сами выбираете себе попутчиков по интересам, для совместных поездок, по предварительной договоренности.<br>
                        Наша цель - сделать Вашу поездку приятной и интересной!<br>
                        Не теряйте время, присоединяйтесь! Выбирайте попутчиков, знакомьтесь, общайтесь и путешествуйте с  удовольствием!
                    </p>
                    <p>
                        С Уважением, автор идеи и основатель клуба «In Train» Усиков Андрей Александрович.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="footer flex-footer">
        <div>
            &copy; intrain.club - 2017. Все права защищены. Правообладатель: Усиков А.А.
        </div>
        <div>
            Разработка сайта: Творческая студия "<a href="#">YB Desing</a>"
        </div>
    </div>
    <!--Register form Start-->
    <div id="register_form" class="form-wrapper" style="display: none;">
        <button class="close_btn" onclick="registerFormToogle()"></button>
        <div class="register-form-body">
            <form>
                <h1 class="form-title text-center uppercase mt0">Регистрация</h1>
                <p class="text-center">Для доступа к личному кабинету пройдите регистрацию</p>
                <div class="form-body">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Ваше имя *">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Ваш email *">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Придумайет пароль *">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Повторите пароль *">
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-info btn-rounded btn-xl btn-block" value="Зарегистрироваться">
                    </div>
                    <div class="flex flex-center">
                        <a href="" class="btn btn-default btn-circle btn-lg btn-social hover_tooltip" data-tooltip="Зарегистрироваться через Viber"><i class="fa fa-whatsapp" aria-hidden="true"></i></a>
                        <a href="" class="btn btn-default btn-circle btn-lg btn-social hover_tooltip" data-tooltip="Зарегистрироваться через ВКонтакте"><i class="fa fa-vk" aria-hidden="true"></i></a>
                        <a href="" class="btn btn-default btn-circle btn-lg btn-social hover_tooltip" data-tooltip="Зарегистрироваться через Facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        <a href="" class="btn btn-default btn-circle btn-lg btn-social hover_tooltip" data-tooltip="Зарегистрироваться через Instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                    </div>
                    <p class="social-label text-center">регистрация через соцсети</p>
                </div>
            </form>
        </div>
    </div>
    <!--Register form End-->

    <!--Authorization form Start-->
    <div id="auth_form" class="form-wrapper" style="display: none;">
        <button class="close_btn" onclick="authFormToogle()"></button>
        <div class="auth-form-body">
            <form>
                <h1 class="form-title text-center uppercase mt0">Личный кабинет</h1>
                <p class="text-center">Для входа в личный кабинет пройдите авторизацию</p>
                <div class="form-body">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Введите email *">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Введите пароль *">
                        <span class="btn btn-remind btn-rounded hover_tooltip" data-x="left" data-y="top" data-color="blue" data-tooltip="Забыли пароль?" onclick="authFormToogle();recovFormToogle();">?</span>

                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-info btn-rounded btn-xl btn-block" value="Войти в кабинет">
                    </div>
                    <div class="flex flex-center">
                        <a href="" class="btn btn-default btn-circle btn-lg btn-social hover_tooltip" data-tooltip="Зарегистрироваться через Viber"><i class="fa fa-whatsapp" aria-hidden="true"></i></a>
                        <a href="" class="btn btn-default btn-circle btn-lg btn-social hover_tooltip" data-tooltip="Зарегистрироваться через ВКонтакте"><i class="fa fa-vk" aria-hidden="true"></i></a>
                        <a href="" class="btn btn-default btn-circle btn-lg btn-social hover_tooltip" data-tooltip="Зарегистрироваться через Facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        <a href="" class="btn btn-default btn-circle btn-lg btn-social hover_tooltip" data-tooltip="Зарегистрироваться через Instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                    </div>
                    <p class="social-label text-center">авторизация через соцсети</p>
                </div>
            </form>
        </div>
    </div>
    <!--Authorization form End-->

    <!--Password recovery form Start-->
    <div id="recov_form" class="form-wrapper" style="display: none;">
        <button class="close_btn" onclick="recovFormToogle()"></button>
        <div class="recov-form-body">
            <form>
                <h1 class="form-title text-center uppercase mt0">Восстановление пароля</h1>
                <p class="text-center">Для восстановления пароля введите Ваш email</p>
                <div class="form-body">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Введите email *">
                    </div>
                    <div class="form-group has-error">
                        <input type="text" class="form-control" placeholder="Введите email *">
                        <p>Ошибка! Это поле не должно быть пустым</p>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-info btn-rounded btn-xl btn-block" value="Восстановить пароль">
                        <a class="btn btn-success btn-rounded btn-xl btn-block" onclick="recovFormToogle();notifFormToogle();">Уведомление</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!--Password recovery form End-->

    <!--Уведомление Start-->
    <div id="notif_form" class="form-wrapper" style="display: none;">
        <button class="close_btn" onclick="notifFormToogle()"></button>
        <div class="notif-form-body">
            <form>
                <h1 class="form-title text-center uppercase mt0">Отправлено!</h1>
                <div class="form-body">
                    <p class="text-center">На Ваш email отправлено письмо с ссылкой для восстановления пароля. Проверьте почту и следуйте указанным инструкциям</p>
                    <div class="form-group text-center">
                        <a class="btn btn-info btn-circle btn-xl" onclick="notifFormToogle()">OK</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!--Уведомление End-->

    <!-- Mainly scripts -->
    <script src="js/jquery-2.1.1.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Peity -->
    <script src="js/plugins/peity/jquery.peity.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="js/inspinia.js"></script>
    <script src="js/plugins/pace/pace.min.js"></script>

    <!-- jQuery UI -->
    <script src="js/plugins/jquery-ui/jquery-ui.min.js"></script>

    <!-- GITTER -->
    <script src="js/plugins/gritter/jquery.gritter.min.js"></script>

    <!-- Sparkline -->
    <script src="js/plugins/sparkline/jquery.sparkline.min.js"></script>

    <!-- ChartJS-->
    <script src="js/plugins/chartJs/Chart.min.js"></script>

    <!-- Toastr -->
    <script src="js/plugins/toastr/toastr.min.js"></script>

    <!-- Revolution slider -->
    <script type="text/javascript" src="js/plugins/slider-revolution/src/js/jquery.themepunch.plugins.min.js"></script>
    <script type="text/javascript" src="js/plugins/slider-revolution/src/js/jquery.themepunch.revolution.min.js"></script>
    <link rel="stylesheet" type="text/css" href="js/plugins/slider-revolution/src/css/settings.css" media="screen" />

    <script>
        function wrapper(){
            var wrapper1 = $('.tp-banner-container');
            var wrapper2 = $('#wrap');
            var wrapper3 = $('#wrapper');
            var winh = $(window).height();
            var headerh = $('nav.navbar').outerHeight();
            var footerh = $('.footer').outerHeight();
            wrapper1.css('height', winh - headerh - footerh);
            wrapper2.css('height', winh - headerh - footerh);
            wrapper3.css('height', winh - footerh);
        }

        var revapi;

        jQuery(document).ready(function() {

                revapi = jQuery('.tp-banner').revolution({
                    delay:60000,
                    autoHeight:"on",
                    hideThumbs:10,
                    forceFullWidth:"on",
                    onHoverStop:"off",
                    fullScreenOffsetContainer:"#tp-banner"
                });

        });

        function aboutBlockToogle(){
            var about = $('#about_block');
            if(about.css('display') == 'none'){
                about.slideDown(500);
            }else{
                about.slideUp(500);
            }
        };

        $(document).ready(function() {
            wrapper();
        });

        $(window).resize(function() {
            wrapper();
        });

    </script>
    <!--Tooltip Start-->
    <div id="top_left_hover_tooltip"></div>
    <div id="top_right_hover_tooltip"></div>
    <div id="bottom_left_hover_tooltip"></div>
    <div id="bottom_right_hover_tooltip"></div>
    <!--Tooltip End-->
</body>
</html>
